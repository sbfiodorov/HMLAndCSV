import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
public class XMLFile
{
    private static final String INPUT_HML = "data/address.xml";
    private static final String ITEM = "item";
    private static final ArrayList<Data> dataArrayList = new ArrayList<>();
    public void parse() throws ParserConfigurationException, IOException, SAXException
    {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new File(INPUT_HML));
        collectInformation(document, ITEM);
    }

    private void collectInformation(Document document, final String element)
    {
        NodeList elements = document.getElementsByTagName(element);
        String city;
        String street;
        String house;
        String floor;
        for (int i = 0; i < elements.getLength(); i++)
        {
            NamedNodeMap attributes = elements.item(i).getAttributes();
            if (ITEM.equals(element))
            {
                city = attributes.getNamedItem("city").getNodeValue();
                street = attributes.getNamedItem("street").getNodeValue();
                house = attributes.getNamedItem("house").getNodeValue();
                floor = attributes.getNamedItem("floor").getNodeValue();
                Data data = new Data(city, street, Integer.parseInt(house), Integer.parseInt(floor));
                dataArrayList.add(data);
            }
        }
        elements = null;
        city = null;
        street = null;
        house = null;
        floor = null;
    }

    public void firstP()
    {
        System.out.println("\nДублирующиеся записи, +, кол-во повторений : ");
        //
        ArrayList<Data> newData = new ArrayList<>(dataArrayList);

        newData.stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet()
                .stream()
                .filter(element -> element.getValue() > 1).toList()
                .forEach(System.out::println);
    }
    public void secondP()
    {
        System.out.println("\nКол-во зданий в каждом городе : ");

        ArrayList<String> firstFloors = new ArrayList<>();
        ArrayList<String> secondFloors = new ArrayList<>();
        ArrayList<String> thirdFloors = new ArrayList<>();
        ArrayList<String> fourFloors = new ArrayList<>();
        ArrayList<String> fiveFloors = new ArrayList<>();

        for (Data data : dataArrayList)
        {
            switch (data.getFloor())
            {
                case 1 -> firstFloors.add(data.getCity());
                case 2 -> secondFloors.add(data.getCity());
                case 3 -> thirdFloors.add(data.getCity());
                case 4 -> fourFloors.add(data.getCity());
                case 5 -> fiveFloors.add(data.getCity());
            }
        }

        System.out.println();
        System.out.println("Первые этажи : ");
        countFrequencies(firstFloors);
        System.out.println();
        System.out.println("Вторые этажи : ");
        countFrequencies(secondFloors);
        System.out.println();
        System.out.println("Третие этажи : ");
        countFrequencies(thirdFloors);
        System.out.println();
        System.out.println("Четвёртые этажи : ");
        countFrequencies(fourFloors);
        System.out.println();
        System.out.println("Пятые этажи : ");
        countFrequencies(fiveFloors);

        firstFloors.clear();
        secondFloors.clear();
        thirdFloors.clear();
        fourFloors.clear();
        fiveFloors.clear();
    }

    public void countFrequencies(ArrayList<String> list)
    {

        Map<String, Integer> hm = new HashMap<>();

        for (String i : list)
        {
            Integer j = hm.get(i);
            hm.put(i, (j == null) ? 1 : j + 1);
        }

        for (Map.Entry<String, Integer> val : hm.entrySet())
        {
            System.out.println(val.getKey() + " : "
                    + " : " + val.getValue());
        }
    }
}