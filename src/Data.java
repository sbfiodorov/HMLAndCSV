import java.util.Objects;

public class Data
{
    private String city;
    private String street;
    private int numberHome;
    private int floor;

    public Data(String city, String street, int numberHome, int floor)
    {
        this.city = city;
        this.street = street;
        this.numberHome = numberHome;
        this.floor = floor;
    }

    public String getCity()
    {
        return city;
    }

    public void setCity(String city)
    {
        this.city = city;
    }

    public String getStreet()
    {
        return street;
    }

    public void setStreet(String street)
    {
        this.street = street;
    }

    public int getNumberHome()
    {
        return numberHome;
    }

    public void setNumberHome(int numberHome)
    {
        this.numberHome = numberHome;
    }

    public int getFloor()
    {
        return floor;
    }

    public void setFloor(int floor)
    {
        this.floor = floor;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Data data = (Data) o;
        return numberHome == data.numberHome && floor == data.floor && Objects.equals(city, data.city) && Objects.equals(street, data.street);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(city, street, numberHome, floor);
    }

    @Override
    public String toString()
    {
        return "Data" +
                "{" +
                "city = " + city +
                ", street = " + street +
                ", numberHome = " + numberHome +
                ", floor = " + floor +
                '}';
    }
}