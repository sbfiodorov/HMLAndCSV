import org.xml.sax.SAXException;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.Scanner;
public class Main
{
    private static final String INPUT_HML = "data/address.xml";
    private static final String INPUT_CSV = "data/address.csv";
    private static final String EXIT = "1";
    public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException
    {
        OneOrTwo();
    }
    public static void OneOrTwo() throws IOException, ParserConfigurationException, SAXException
    {
        CSVFile c = new CSVFile();
        XMLFile x = new XMLFile();
        Scanner scanner = new Scanner(System.in);

        while (true)
        {
            System.out.println("\nЧтобы выйти из программы, нажмите - 1");
            System.out.println("\nВведите путь к файлу : ");
            String str = scanner.next();

            switch (str)
            {
                case EXIT ->
                {
                    scanner.close();
                    System.exit(1);
                }
                case INPUT_CSV ->
                {
                    System.out.println("\nВы перешли к csv файлу");
                    long start = System.currentTimeMillis();
                    c.parse();
                    c.firstP();
                    c.secondP();
                    long end = System.currentTimeMillis();
                    System.out.println("\nВремя обработки файла : " + (end - start));
                }
                case INPUT_HML ->
                {
                    System.out.println("\nВы перешли к xml файлу");
                    long start2 = System.currentTimeMillis();
                    x.parse();
                    x.firstP();
                    x.secondP();
                    long end2 = System.currentTimeMillis();
                    System.out.println("\nВремя обработки файла : " + (end2 - start2));
                }
            }
        }
    }
}