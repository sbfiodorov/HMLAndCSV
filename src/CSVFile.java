import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
public class CSVFile
{
    private static final String INPUT_CSV = "data/address.csv";
    private static final ArrayList<Data> dataArrayList = new ArrayList<>();
    public void parse() throws IOException
    {
        List<String> list2 = Files.readAllLines(Path.of(INPUT_CSV));
        for(int j = 1; j < list2.size(); j++)
        {
            String[] f2 = list2.get(j).split(";");
            Data data = new Data(f2[0], f2[1], Integer.parseInt(f2[2]), Integer.parseInt(f2[3]));
            dataArrayList.add(data);
        }
        list2.clear();
    }
    public void firstP()
    {
        System.out.println("\nДублирующиеся записи, +, кол-во повторений : ");
        //
        ArrayList<Data> newData = new ArrayList<>(dataArrayList);

        newData.stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet()
                .stream()
                .filter(element -> element.getValue() > 1).toList()
                .forEach(System.out::println);
    }
    public void secondP()
    {
        System.out.println("\nКол-во зданий в каждом городе : ");

        ArrayList<String> firstFloors = new ArrayList<>();
        ArrayList<String> secondFloors = new ArrayList<>();
        ArrayList<String> thirdFloors = new ArrayList<>();
        ArrayList<String> fourFloors = new ArrayList<>();
        ArrayList<String> fiveFloors = new ArrayList<>();

        for (Data data : dataArrayList)
        {
            switch (data.getFloor())
            {
                case 1 -> firstFloors.add(data.getCity());
                case 2 -> secondFloors.add(data.getCity());
                case 3 -> thirdFloors.add(data.getCity());
                case 4 -> fourFloors.add(data.getCity());
                case 5 -> fiveFloors.add(data.getCity());
            }
        }

        System.out.println();
        System.out.println("Первые этажи : ");
        countFrequencies(firstFloors);
        System.out.println();
        System.out.println("Вторые этажи : ");
        countFrequencies(secondFloors);
        System.out.println();
        System.out.println("Третие этажи : ");
        countFrequencies(thirdFloors);
        System.out.println();
        System.out.println("Четвёртые этажи : ");
        countFrequencies(fourFloors);
        System.out.println();
        System.out.println("Пятые этажи : ");
        countFrequencies(fiveFloors);

        firstFloors.clear();
        secondFloors.clear();
        thirdFloors.clear();
        fourFloors.clear();
        fiveFloors.clear();
    }

    public void countFrequencies(ArrayList<String> list)
    {
        Map<String, Integer> hm = new HashMap<>();

        for (String i : list)
        {
            Integer j = hm.get(i);
            hm.put(i, (j == null) ? 1 : j + 1);
        }

        for (Map.Entry<String, Integer> val : hm.entrySet())
        {
            System.out.println(val.getKey() + " : "
                    + " : " + val.getValue());
        }
    }
}